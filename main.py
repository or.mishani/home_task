from fastapi import FastAPI, UploadFile, File, HTTPException, Form
from collections import Counter
from celery import Celery
from typing import List
import os
import random
from prometheus_client import start_http_server, Counter as PrometheusCounter, Gauge
import threading
import logging

# Initialize FastAPI app
app = FastAPI()

# Initialize Celery
celery_app = Celery(
    'text_processor',
    broker='redis://redis:6379/0',
    backend='redis://redis:6379/0'
)

# Prometheus metrics
total_requests = PrometheusCounter('total_requests', 'Total number of requests')
selected_files_gauge = Gauge('selected_files', 'Number of selected files')
common_word_count_gauge = Gauge('common_word_count', 'Count of the most common word', ['word'])

# Celery task to process text documents
@celery_app.task
def process_text(file_paths: List[str]) -> dict:
    all_words = []
    for file_path in file_paths:
        with open(file_path, 'r') as file:
            text = file.read()
            words = text.split()
            all_words.extend(words)
    
    # Count the occurrences of each word
    word_counts = Counter(all_words)
    
    # Return the 2 most common words
    return word_counts.most_common(2)

from celery.result import AsyncResult

@app.get("/process")
async def process_from_directory():
    total_requests.inc()
    directory = "/usr/src/app/documents"
    # List all files in the given directory
    all_files = [f for f in os.listdir(directory) if os.path.isfile(os.path.join(directory, f))]

    # Randomly select 4 files (or fewer if there are not enough files)
    selected_files = random.sample(all_files, min(4, len(all_files)))
    selected_files_gauge.set(len(selected_files))
    # Generate full paths for the selected files
    file_paths = [os.path.join(directory, f) for f in selected_files]

    # Asynchronously process the text documents using Celery
    task = process_text.delay(file_paths)
    result = task.get()
    # Wait for the task to complete and get the results
    result = AsyncResult(task.id).get()
    for word, count in result:
        common_word_count_gauge.labels(word=word).set(count)
    return {"result": result, "selected_files": selected_files}

def start_metrics_server():
    try:
        start_http_server(8001)
        logging.info("Prometheus metrics server started on port 8001")
    except Exception as e:
        logging.error(f"Error starting Prometheus metrics server: {e}")

# Start the Prometheus metrics server in a separate thread
threading.Thread(target=start_metrics_server, daemon=True).start()

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
